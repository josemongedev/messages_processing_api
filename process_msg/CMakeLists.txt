project(process_msg LANGUAGES C)

set(PROCESS_MSG_SRCS "process_app.c")
add_executable(process_app ${PROCESS_MSG_SRCS}) 
target_link_libraries(process_app PUBLIC  ${MESSAGES_API_SRCS}) 
