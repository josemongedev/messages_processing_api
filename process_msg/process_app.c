#include "messages/message_types.h"
#include <messages/message_input.h>
#include <messages/message_output.h>
#include <stdlib.h>

int run_io_process() {
  input_pair_t input_pair = {0};

  const inp_status_t inp_status = read_input_pair(&input_pair);

  if (inp_status != INPUT_SUCCESS) {
    write_error_output(input_error_message[inp_status]);
    return EXIT_FAILURE;
  }

  message_out_t message_out;
  process_message(&input_pair, &message_out);

  const output_status_t out_status = write_message_output(message_out);
  if (out_status != OUTPUT_SUCCESS)
    return EXIT_FAILURE;

  return EXIT_SUCCESS;
}

int main() { return run_io_process(); }