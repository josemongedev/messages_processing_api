#! /bin/bash

echo "Starting build..."
mkdir -p build
cd build
cmake ..
THREADS=`nproc`
echo "Building with $THREADS threads:"
make -j$THREADS
cd ..
cp build/tests/message_tests .
cp build/tests/input_generator .
cp build/process_msg/process_app .
echo "Build...DONE"
./input_generator