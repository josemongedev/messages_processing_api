#include "helpers.cpp"
#include <gtest/gtest.h>

extern "C" {
#include <messages/message_input.h>
}

class MessageInputTest : public testing::Test {
  void SetUp() override {
    const bool test_file_create_success =
        create_raw_sample_input_file(true, true);
    GTEST_ASSERT_TRUE(test_file_create_success);
  }
  void TearDown() override { delete_sample_file(); }
};

TEST_F(MessageInputTest, HandlesRawInputRead) {
  // Size macros include both the null termination character and padding used
  // for message alignment (but later unused)
  char message[MESSAGE_ASCII_WITH_LEAD_SIZE + ALIGNMENT_PADDING_BYTES];
  char mask[MASK_ASCII_WITH_LEAD_SIZE];
  inp_status_t raw_input_status = read_raw_input_frame(message, mask);

  GTEST_ASSERT_EQ(raw_input_status, INPUT_SUCCESS);
}

TEST_F(MessageInputTest, HandlesPairInputRead) {
  input_pair_t input_pair = {0};
  const inp_status_t pair_input_status = read_input_pair(&input_pair);
  GTEST_ASSERT_EQ(pair_input_status, INPUT_SUCCESS);
}
