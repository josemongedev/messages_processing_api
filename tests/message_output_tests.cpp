#include "helpers.cpp"
#include <gtest/gtest.h>

extern "C" {

#include <messages/message_input.h>
#include <messages/message_output.h>
#include <string.h>
}

class MessageOutputTest : public testing::Test {
  void SetUp() override {
    const bool test_file_create_success =
        create_raw_sample_input_file(true, true);
    GTEST_ASSERT_TRUE(test_file_create_success);
  }
  void TearDown() override { delete_sample_file(); }
};

TEST_F(MessageOutputTest, HandlesTetradsMask) {
  input_pair_t input_pair = {0};

  const inp_status_t pair_input_status = read_input_pair(&input_pair);
  GTEST_ASSERT_EQ(pair_input_status, INPUT_SUCCESS);

  message_out_t message_out = {0};
  process_message(&input_pair, &message_out);
}

TEST_F(MessageOutputTest, HandlesOutputWrite) {
  input_pair_t input_pair = {0};

  const inp_status_t pair_input_status = read_input_pair(&input_pair);
  GTEST_ASSERT_EQ(pair_input_status, INPUT_SUCCESS);

  if (pair_input_status != INPUT_SUCCESS)
    write_error_output(input_error_message[pair_input_status]);

  message_out_t message_out = {0};
  process_message(&input_pair, &message_out);

  write_message_output(message_out);
}