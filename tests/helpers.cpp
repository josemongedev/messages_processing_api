#ifndef HELPERS_CPP
#define HELPERS_CPP

extern "C" {
#include <limits.h>
#include <messages/ascii_convert.h>
#include <messages/crc_32.h>
#include <messages/message_input.h>
#include <messages/message_types.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
}

/** HELPER FUNCTIONS **/
message_padded_t generate_empty_frame() {
  message_padded_t padded_frame;
  memset(&padded_frame, 0, sizeof(message_padded_t));
  return padded_frame;
}

message_padded_t generate_binary_frame(uint8_t type, uint8_t length,
                                       uint8_t data_value, uint16_t padding) {
  message_padded_t padded_frame;
  memset(&padded_frame, 0, sizeof(message_padded_t));
  padded_frame.padding = padding;
  padded_frame.message.type = type;
  padded_frame.message.length = length;
  memset(&padded_frame.message.payload.data, data_value, length);
  padded_frame.message.payload.CRC32 = calculate_data_CRC_32(
      padded_frame.message.payload.data, padded_frame.message.length);
  return padded_frame;
}

// Type can only be between 0 and F
// Length can only be between 0 and F'
// Padding value sets the same character for all padding
// Data value sets the same character for all data
void fill_custom_ascii_frame(uint8_t type, uint8_t length, char data_value,
                             uint8_t data[], char padding_value,
                             char *ascii_expected) {
  // Fill with zeroes
  memset(ascii_expected, (int)'0', MESSAGE_ASCII_PADDED_SIZE);
  // Fill the type
  memset(&ascii_expected[1], (type + '0'), 1);
  // Fill the length
  memset(&ascii_expected[3], (length + '0'), 1);
  // Fill the data values
  memset(&ascii_expected[4], data_value, length * 2);
  // Fill the CRC
  uint32_t binary_crc = calculate_data_CRC_32(data, length);
  char ascii_crc[MASK_ASCII_SIZE + 1];
  convert_frame_to_ascii_hex(&binary_crc, 4, ascii_crc);
  memcpy(&ascii_expected[MESSAGE_ASCII_PADDED_SIZE - 12], &ascii_crc, 8);
  // Fill the padding (unused value, but checking for validation purposes)
  memset(&ascii_expected[MESSAGE_ASCII_PADDED_SIZE - 4], 'A', 4);
  ascii_expected[MESSAGE_ASCII_PADDED_SIZE] = '\0';
}

void fill_empty_ascii_frame(char *ascii_expected) {
  // Fill with zeroes
  memset(ascii_expected, (int)'0', MESSAGE_ASCII_PADDED_SIZE);
  ascii_expected[MESSAGE_ASCII_PADDED_SIZE] = '\0';
}

// Creates raw message ascii string meaning:
// leading string prefix + ASCII Hex converted message
void create_raw_message_ascii_hex_str(message_padded_t *padded_frame,
                                      char *message_raw) {
  char message_ascii[MESSAGE_ASCII_PADDED_SIZE + 1];
  // Generate first a message ascii string with leading string to be parsed
  convert_frame_to_ascii_hex((uint32_t *)padded_frame, MESSAGE_PADDED_SIZE,
                             message_ascii);
  strcat(message_raw, leading_message_pattern);
  strcat(message_raw, message_ascii);
}

void create_raw_mask_ascii_hex_str(mask_t mask_expected, char *mask_raw) {
  // Generate first a mask ascii string with leading string to be parsed
  char mask_ascii[MASK_ASCII_SIZE + 1] = "";
  convert_mask_to_ascii_hex((uint32_t)mask_expected, mask_ascii);
  strcat(mask_raw, leading_mask_pattern);
  strcat(mask_raw, mask_ascii);
}

// Both add_message and add_mask flags toggle whether message or mask are
// written or not to file
// Return false if there was an error, true otherwise
bool create_raw_sample_input_file(bool add_message, bool add_mask) {
#ifdef TESTING_FLAG
#define TEST_PREFIX "test_"
#else
#define TEST_PREFIX ""
#endif
  char sample_file_path[sizeof(TEST_PREFIX) + sizeof(INPUT_FILENAME) + 1] =
      TEST_PREFIX;
  strcat(sample_file_path, INPUT_FILENAME);

  FILE *sample_input = fopen(sample_file_path, "w");
  if (!sample_input)
    return false;

  char message_raw[MESSAGE_ASCII_PADDED_WITH_LEAD_SIZE + 1] = {'\0'};
  message_padded_t padded_frame = generate_binary_frame(1, 2, 0xDD, 0xAAAA);
  create_raw_message_ascii_hex_str(&padded_frame, message_raw);

  bool write_failure;
  if (add_message) {
    message_raw[MESSAGE_ASCII_PADDED_WITH_LEAD_SIZE - 5] =
        '\0'; // Removes padding
    write_failure = fprintf(sample_input, "%s\n", message_raw) !=
                    MESSAGE_ASCII_PADDED_WITH_LEAD_SIZE - 4;
  }

  char mask_raw[MASK_ASCII_WITH_LEAD_SIZE + 1] = {'\0'};
  uint32_t mask_expected = 0x01ABCDEF;
  create_raw_mask_ascii_hex_str(mask_expected, mask_raw);

  if (add_mask && !write_failure)
    write_failure =
        fprintf(sample_input, "%s\n", mask_raw) != MASK_ASCII_WITH_LEAD_SIZE;

  return (fclose(sample_input) != EOF) && (!write_failure);
}

void delete_sample_file() {
  char sample_file_path[sizeof(TEST_PREFIX) + sizeof(INPUT_FILENAME) + 1] =
      TEST_PREFIX;
  strcat(sample_file_path, INPUT_FILENAME);
  // printf("Sample file to delete: %s\n", sample_file_path);
  const int delete_error = remove(sample_file_path);
  if (delete_error) {
    perror("Couldn't delete sample input file");
    return;
  }
}
#endif /* HELPERS_CPP */
