#include "helpers.cpp"
#include <gtest/gtest.h>

extern "C" {
#include <messages/ascii_convert.h>
}

class ASCIIConvertTest : public testing::Test {
protected:
  void SetUp() override {}
};

// ascii_convert.h tests
TEST_F(ASCIIConvertTest, HandlesAsciiMaskToBinary) {
  mask_t mask;
  convert_ascii_hex_to_mask((char *)"0ABCDEFF", &mask);
  GTEST_ASSERT_EQ(mask, 0x0ABCDEFF);
}

// Mask conversion from binary to ascii and back to binary
TEST_F(ASCIIConvertTest, HandlesMaskToAsciiHex) {
  mask_t mask = 0xFFDEBC0A;
  char ascii_buffer[MASK_ASCII_SIZE + 1];

  convert_mask_to_ascii_hex(mask, ascii_buffer);
  ASSERT_STREQ(ascii_buffer, "FFDEBC0A");

  convert_ascii_hex_to_mask(ascii_buffer, &mask);
  GTEST_ASSERT_EQ(mask, 0xFFDEBC0A);
}

// Message conversion from binary to ascii and back to binary
// ASCII_MESSAGE_OUTPUT_SIZE
TEST_F(ASCIIConvertTest, HandlesMessageToAsciiHex) {
  message_padded_t padded_frame = generate_empty_frame();
  char ascii_expected[MESSAGE_ASCII_PADDED_SIZE + 1];
  fill_empty_ascii_frame(ascii_expected);

  char ascii_message[MESSAGE_ASCII_PADDED_SIZE + 1];
  convert_frame_to_ascii_hex((uint32_t *)&padded_frame, MESSAGE_PADDED_SIZE,
                             ascii_message);
  ASSERT_STREQ(ascii_message, ascii_expected);
}

TEST_F(ASCIIConvertTest, HandlesAsciiMessageToBinary) {
  char ascii_expected[MESSAGE_ASCII_PADDED_SIZE + 1];
  uint8_t data[] = {0xDD, 0xDD};
  fill_custom_ascii_frame(0x1, 0x2, 'D', data, 'A', ascii_expected);

  message_padded_t expected_padded_frame =
      generate_binary_frame(1, 2, 0xDD, 0xAAAA);

  char ascii_message[MESSAGE_ASCII_PADDED_SIZE + 1];
  convert_frame_to_ascii_hex((uint32_t *)&expected_padded_frame,
                             MESSAGE_PADDED_SIZE, ascii_message);
  ASSERT_STREQ(ascii_message, ascii_expected);

  message_padded_t converted_padded_frame;
  memset(&converted_padded_frame, 0, MESSAGE_PADDED_SIZE);
  convert_ascii_hex_to_frame(ascii_message,
                             (uint32_t *)&converted_padded_frame);

  GTEST_ASSERT_EQ(converted_padded_frame.message.length,
                  expected_padded_frame.message.length);
  GTEST_ASSERT_EQ(converted_padded_frame.message.type,
                  expected_padded_frame.message.type);
  GTEST_ASSERT_EQ(converted_padded_frame.message.payload.CRC32,
                  expected_padded_frame.message.payload.CRC32);

  for (uint8_t data_idx = 0; data_idx < expected_padded_frame.message.length;
       data_idx++) {
    GTEST_ASSERT_EQ(converted_padded_frame.message.payload.data[data_idx],
                    expected_padded_frame.message.payload.data[data_idx]);
  }

  char converted_asciii[MESSAGE_ASCII_PADDED_SIZE + 1];
  convert_frame_to_ascii_hex((uint32_t *)&converted_padded_frame,
                             MESSAGE_PADDED_SIZE, converted_asciii);
  ASSERT_STREQ(ascii_expected, converted_asciii);
}
