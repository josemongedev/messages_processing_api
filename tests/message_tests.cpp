#include "ascii_convert_tests.cpp"
#include "crc_32_tests.cpp"
#include "mask_parse_tests.cpp"
#include "message_input_tests.cpp"
#include "message_output_tests.cpp"
#include "message_parse_tests.cpp"
#include <gtest/gtest.h>

int main(int argc, char *argv[]) {
  // printf("Testing enabled: %u", (TESTING_FLAG));
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}