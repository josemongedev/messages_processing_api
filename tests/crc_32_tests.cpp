#include <gtest/gtest.h>

extern "C" {
#include <messages/crc_32.h>
}

class CRC32Test : public testing::Test {
protected:
  void SetUp() override {}
};

// crc_32.h tests
TEST(CRC32Test, HandlesSingleByteInput) {
  // One byte
  char data1[1] = {'A'};
  GTEST_ASSERT_EQ(0xD3D99E8B, calculate_data_CRC_32((uint8_t *)data1, 1));
}

TEST(CRC32Test, HandlesWord32bitInput) {
  // 4 bytes (32bit)
  char data2[4] = {'A', 'B', 'C', 'D'};
  GTEST_ASSERT_EQ(0xDB1720A5, calculate_data_CRC_32((uint8_t *)data2, 4));
}