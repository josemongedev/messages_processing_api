#include "helpers.cpp"
#include <gtest/gtest.h>

extern "C" {
#include <endian.h>
#include <messages/ascii_convert.h>
#include <messages/message_parse.h>
}

class MessageParseTest : public testing::Test {};

TEST_F(MessageParseTest, HandlesLeadingStringCheckValid) {
  char ascii_line[7] = "mess=*";
  char *message_start_ptr = NULL;
  inp_status_t check_status =
      check_message_lead_str(ascii_line, &message_start_ptr);
  // After the leading string, there is only a null string termination character
  GTEST_ASSERT_EQ(check_status, INPUT_SUCCESS);
  GTEST_ASSERT_EQ(*message_start_ptr, '*');
}

TEST_F(MessageParseTest, HandlesLeadingStringCheckInvalid) {
  char ascii_line[6] = "XXXX";
  char *message_start_ptr = NULL;
  inp_status_t check_status =
      check_message_lead_str(ascii_line, &message_start_ptr);
  // After the leading string, there is only a null string termination character
  GTEST_ASSERT_EQ(check_status, INPUT_MESSAGE_LEAD_MISSING_ERROR);
}

// TEST_F(MessageParseTest, HandlesEndinanessToggle) {
//   printf("%08X\n", 0x01234567);
//   printf("%08X\n", htobe32(htobe32(0x01234567)));
//   GTEST_ASSERT_EQ(htobe32((htobe32((0x01234567)))), 0x01234567);
// }

TEST_F(MessageParseTest, HandlesParseMessage) {
  message_padded_t padded_frame = generate_binary_frame(1, 2, 0xDD, 0xAAAA);

  char message_raw[MESSAGE_ASCII_PADDED_WITH_LEAD_SIZE + 1] = {'\0'};
  create_raw_message_ascii_hex_str(&padded_frame, message_raw);

  message_padded_t parsed_padded_frame;
  const inp_status_t status =
      parse_input_message(message_raw, &parsed_padded_frame);
  GTEST_ASSERT_EQ(status, INPUT_SUCCESS);
  GTEST_ASSERT_EQ(parsed_padded_frame.message.length,
                  padded_frame.message.length);
  GTEST_ASSERT_EQ(parsed_padded_frame.message.type, padded_frame.message.type);
  GTEST_ASSERT_EQ(parsed_padded_frame.message.payload.CRC32,
                  padded_frame.message.payload.CRC32);

  for (uint8_t data_idx = 0; data_idx < padded_frame.message.length;
       data_idx++) {
    GTEST_ASSERT_EQ(parsed_padded_frame.message.payload.data[data_idx],
                    padded_frame.message.payload.data[data_idx]);
  }
}
