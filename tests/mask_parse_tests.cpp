#include "helpers.cpp"
#include <gtest/gtest.h>

extern "C" {
#include <messages/ascii_convert.h>
#include <messages/mask_parse.h>
}

class MaskParseTest : public testing::Test {};

TEST_F(MaskParseTest, HandlesLeadingStringCheckValid) {
  char ascii_line[7] = "mask=*";
  char *mask_start_ptr = NULL;
  inp_status_t check_status = check_mask_lead_str(ascii_line, &mask_start_ptr);
  // After the leading string, there is only a null string termination character
  GTEST_ASSERT_EQ(check_status, INPUT_SUCCESS);
  GTEST_ASSERT_EQ(*mask_start_ptr, '*');
}

TEST_F(MaskParseTest, HandlesLeadingStringCheckInvalid) {
  char ascii_line[6] = "XXXX";
  char *mask_start_ptr = NULL;
  inp_status_t check_status = check_mask_lead_str(ascii_line, &mask_start_ptr);
  // After the leading string, there is only a null string termination character
  GTEST_ASSERT_EQ(check_status, INPUT_MASK_LEAD_MISSING_ERROR);
}

TEST_F(MaskParseTest, HandlesParseMask) {
  char mask_raw[MASK_ASCII_WITH_LEAD_SIZE + 1] = {'\0'};

  uint32_t mask_expected = 0x01ABCDEF;
  create_raw_mask_ascii_hex_str(mask_expected, mask_raw);

  uint32_t parsed_mask;
  const inp_status_t status = parse_input_mask(mask_raw, &parsed_mask);
  GTEST_ASSERT_EQ(status, INPUT_SUCCESS);
  GTEST_ASSERT_EQ(parsed_mask, 0x01ABCDEF);
}