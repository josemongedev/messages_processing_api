#! /bin/bash


FILE=./message_tests
if test -f "$FILE"; then
    echo "Running."
    exec $FILE 
else
    echo "Binary not found"
    echo "INFO: Run build.sh first in order to compile the project, and then run this script."
    echo ""
fi
