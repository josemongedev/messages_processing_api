#ifndef MESSAGE_PARSE_H
#define MESSAGE_PARSE_H

#include <messages/ascii_convert.h>
#include <messages/crc_32.h>
#include <messages/message_types.h>

#define MESSAGE_LEAD_STR "mess="
#define LEAD_MESSAGE_LENGTH (sizeof(MESSAGE_LEAD_STR))

extern const char leading_message_pattern[];

inp_status_t check_message_lead_str(char *ascii_line, char **message_start_ptr);

inp_status_t zero_pad_payload_data(message_t *message);

inp_status_t parse_input_message(char *message_raw, message_padded_t *message);

#endif /* MESSAGE_PARSE_H */
