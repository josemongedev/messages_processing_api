#include <endian.h>
#include <messages/ascii_convert.h>
#include <stdio.h>

uint32_t toggle_endianness(uint32_t wordu32) { return htobe32(wordu32); }

void convert_ascii_hex_to_mask(char *ascii_buffer, uint32_t *mask) {
  // 8 ASCII chars make up 32 bits/4 bytes
  *mask = strtol(ascii_buffer, NULL, 16);
}

void convert_mask_to_ascii_hex(uint32_t mask, char *ascii_buffer) {
  snprintf(ascii_buffer, 9, "%08X", mask);
}

void convert_word32_to_ascii_hex(uint32_t frame_word, char *ascii_word32) {
  snprintf(ascii_word32, ASCII_CHARS_WORD32 + 1, "%08X",
           toggle_endianness(frame_word));
}

uint32_t get_word32_from_ascii_hex(char *ascii_word32) {
  return toggle_endianness(strtol(ascii_word32, NULL, 16));
}

// Copying 4 bytes(16 bits) at a time, because entire message is 258
// bytes, so in total 516 ASCII characters(divisible by 4, but not 8)
void convert_ascii_hex_to_frame(char *ascii_buffer, uint32_t *frame) {
  char ascii_word32[ASCII_CHARS_WORD32 + 1] = {[ASCII_CHARS_WORD32] = '\0'};
  // Since 2 ASCII characters describe one byte, increase index by 2
  size_t words_total = (MESSAGE_PADDED_SIZE >> 2);
  for (size_t word_idx = 0; word_idx < words_total; word_idx++) {
    memcpy(ascii_word32, ascii_buffer, ASCII_CHARS_WORD32);
    // strtol should always stop by the '\0' character, which isn't ever
    // overwritten, since memcpy only copies 8 bytes
    *frame = get_word32_from_ascii_hex(ascii_word32);
    ascii_buffer += ASCII_CHARS_WORD32;
    frame++;
  }
}

void convert_frame_to_ascii_hex(uint32_t *frame, size_t bytes_length,
                                char *ascii_buffer) {
  // This loop could probably be improved by printing 8 ASCII chars at a time
  // (32 bits message output pointer)
  char ascii_word32[ASCII_CHARS_WORD32 + 1] = {[ASCII_CHARS_WORD32] = '\0'};
  // Divide by 4 to convert to 32 bit words (4 bytes) total count
  size_t words_total = (bytes_length >> 2);
  for (size_t word32_idx = 0; word32_idx < words_total; word32_idx++) {
    convert_word32_to_ascii_hex(frame[word32_idx], ascii_word32);
    memcpy(ascii_buffer, ascii_word32, ASCII_CHARS_WORD32);
    ascii_buffer += ASCII_CHARS_WORD32;
  }
  // Finish with null character manually on last position
  *ascii_buffer = '\0';
}