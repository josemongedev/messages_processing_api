#include "messages/message_types.h"
#include <messages/message_input.h>
#include <stdio.h>
#include <string.h>

const char *input_error_message[INPUT_TOTAL_ERROR_TYPES] = {
    "Success",
    "Error: While opening input file, make sure it exists first",
    "Error: Too short or data missing",
    "Error: While closing file, data could have been lost",
    "Error: Message Leading Missing",
    "Error: Mask Leading Missing",
    "Error: Leading string Missing",
    "Error: Invalid Message Length",
    "Error: Invalid Mask Length",
    "Error: Invalid Payload Data Length Format",
    "Error: CRC Mismatch, data is corrupted",
};

inp_status_t read_raw_input_frame(char *message, char *mask) {
#ifdef TESTING_FLAG
#define TEST_PREFIX "test_"
#else
#define TEST_PREFIX ""
#endif
  char sample_file_path[sizeof(TEST_PREFIX) + sizeof(INPUT_FILENAME) + 1] =
      TEST_PREFIX;
  strcat(sample_file_path, INPUT_FILENAME);
  FILE *input_source = fopen(sample_file_path, "r");
  if (!input_source) {
    perror("Couldn't open test file");
    return INPUT_OPEN_ERROR;
  }

  // Read message line first from file
  ssize_t read_messages_bytes =
      fread(message, 1, MESSAGE_ASCII_WITH_LEAD_SIZE, input_source);
  // Replace newline character, leave just the ASCII hex message
  message[MESSAGE_ASCII_WITH_LEAD_SIZE - 1] = '\0';
  // Padding necessary initially for alignment purposes (when toggling
  // endianness)
  strcat(message, "0000");
  if (!read_messages_bytes ||
      read_messages_bytes != (MESSAGE_ASCII_WITH_LEAD_SIZE))
    return INPUT_INVALID_MESSAGE_LENGTH;

  // fread(NULL, size_t size, size_t n, FILE *restrict stream)
  // Read mask line afterwards
  ssize_t read_mask_bytes =
      fread(mask, 1, MASK_ASCII_WITH_LEAD_SIZE, input_source);
  mask[MASK_ASCII_WITH_LEAD_SIZE] = '\0';
  if (!read_mask_bytes || read_mask_bytes != (MASK_ASCII_WITH_LEAD_SIZE))
    return INPUT_INVALID_MASK_LENGTH;

  const int close_error = fclose(input_source);
  if (close_error)
    return INPUT_CLOSE_ERROR;
  return INPUT_SUCCESS;
}

inp_status_t read_input_pair(input_pair_t *pair) {
  // +2 for newline and null characters, discarded after parsing
  char message[MESSAGE_ASCII_WITH_LEAD_SIZE + 2];
  char mask[MASK_ASCII_WITH_LEAD_SIZE + 2];

  const inp_status_t raw_error = read_raw_input_frame(message, mask);
  if (raw_error)
    return raw_error;

  const inp_status_t msg_error = parse_input_message(message, &pair->message);
  if (msg_error)
    return msg_error;

  const inp_status_t mask_error = parse_input_mask(mask, &pair->mask);
  if (mask_error)
    return mask_error;

  return INPUT_SUCCESS;
}
