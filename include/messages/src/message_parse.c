#include "messages/message_types.h"
#include <messages/message_parse.h>
#include <stdio.h>

const char leading_message_pattern[] = MESSAGE_LEAD_STR;

inp_status_t check_message_lead_str(char *ascii_line,
                                    char **message_start_ptr) {
  // Checks for leading string presence
  char *lead_str_ptr = strstr(ascii_line, leading_message_pattern);
  if (!lead_str_ptr)
    return INPUT_MESSAGE_LEAD_MISSING_ERROR;

  // Return the start pointer for the ASCII message
  *message_start_ptr = lead_str_ptr + LEAD_MESSAGE_LENGTH - 1;
  return INPUT_SUCCESS;
}

inp_status_t zero_pad_payload_data(message_t *message) {
  const size_t padding_length = PAYLOAD_DATA_LENGTH - message->length;
  // No padding needed
  if (padding_length == 0)
    return INPUT_SUCCESS;
  // Length greater than the maximum payload data size corresponds to a
  // malformed input frame
  if (message->length > PAYLOAD_DATA_LENGTH)
    return INPUT_INVALID_PAYLOAD_DATA_LENGTH_FORMAT;

  const size_t padding_start_idx = message->length + 1;
  memset(&(message->payload.data[padding_start_idx]), 0, padding_length);
  return INPUT_SUCCESS;
}

inp_status_t parse_input_message(char *message_raw, message_padded_t *frame) {
  // Check if message is at least the right size and has "mess=" as leading
  // characters, and get the ascii message buffer pointer
  char *message_ascii_ptr = NULL;
  inp_status_t valid_status =
      check_message_lead_str(message_raw, &message_ascii_ptr);
  if (valid_status != INPUT_SUCCESS)
    return valid_status;

  // Parse all ASCII characters to bytes
  convert_ascii_hex_to_frame(message_ascii_ptr, (uint32_t *)frame);

  // Length cannot exceed the maximum payload data bytes
  if (frame->message.length > 252)
    return INPUT_INVALID_PAYLOAD_DATA_LENGTH_FORMAT;

  // CRC on data checked against received CRC
  const uint32_t calculated_crc =
      calculate_data_CRC_32(frame->message.payload.data, frame->message.length);

  if (calculated_crc != frame->message.payload.CRC32)
    return INPUT_CRC_MISMATCH;
  return INPUT_SUCCESS;
}