#include <messages/crc_32.h>
#include <stdio.h>

uint32_t get_byte_CRC_32(uint32_t crc_register, uint8_t data_byte) {
  crc_register ^= (uint32_t)data_byte;

  for (size_t bit_idx = 0; bit_idx < BYTE_8_BITS_LONG; bit_idx++) {
    const bool lsb_bit = crc_register & LSB_BIT_32;
    crc_register >>= 1;
    if (lsb_bit)
      crc_register ^= REVERSED_POLYNOMIAL;
  }

  return crc_register;
}

uint32_t calculate_data_CRC_32(uint8_t data[], size_t length) {
  uint32_t CRC_32 = CRC32_SEED;
  for (size_t data_idx = 0; data_idx < length; data_idx++)
    CRC_32 = get_byte_CRC_32(CRC_32, data[data_idx]);

  // Return 1's complement
  return ~CRC_32;
}