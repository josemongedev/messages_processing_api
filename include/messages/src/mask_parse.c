#include <messages/mask_parse.h>

const char leading_mask_pattern[] = MASK_LEAD_STR;

inp_status_t check_mask_lead_str(char *ascii_line, char **mask_start_ptr) {
  // Checks for leading string presence
  char *lead_str_ptr = strstr(ascii_line, leading_mask_pattern);
  if (!lead_str_ptr)
    return INPUT_MASK_LEAD_MISSING_ERROR;
  // Return the start pointer for the ASCII mask
  *mask_start_ptr = lead_str_ptr + LEAD_MASK_LENGTH - 1;
  return INPUT_SUCCESS;
}

inp_status_t parse_input_mask(char *mask_raw, uint32_t *mask) {
  char *mask_ascii_ptr = NULL;

  inp_status_t check_error = check_mask_lead_str(mask_raw, &mask_ascii_ptr);
  if (check_error)
    return check_error;

  convert_ascii_hex_to_mask(mask_ascii_ptr, mask);
  return INPUT_SUCCESS;
}