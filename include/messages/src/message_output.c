#include <messages/message_output.h>

void process_message(
    input_pair_t *input_pair,
    message_out_t *message_out) { // Copy the original into both, and then
                                  // modify the masked one
  memcpy(&message_out->original_message, &input_pair->message.message,
         sizeof(message_t));
  memcpy(&message_out->masked_message, &input_pair->message.message,
         sizeof(message_t));

  uint32_t *tetrad_ptr = (uint32_t *)message_out->masked_message.payload.data;
  // PAYLOAD_DATA_LENGTH is divided by 4 to get total tetrads, and then divided
  // by 2(integer division) to get the total even ones, so finally divided by 8,
  // which is equal to shifting-right 3 times (multiply by 2^-3).
  for (size_t tetrad_idx = 0; tetrad_idx <= (PAYLOAD_DATA_LENGTH >> 3);
       // Starting from 0 (even), skip odd tetrads(increase by 2)
       tetrad_idx += 2)
    tetrad_ptr[tetrad_idx] &= input_pair->mask;

  message_out->masked_message.payload.CRC32 =
      calculate_data_CRC_32(message_out->masked_message.payload.data,
                            message_out->masked_message.length);
}

output_status_t write_ascii_output(uint32_t *binary, FILE *output_file) {
  // size + 1 for '/0' string termination character
  char ascii_output_buffer[ASCII_MESSAGE_OUTPUT_SIZE + 1];

  convert_frame_to_ascii_hex(binary, MESSAGE_OUTPUT_SIZE, ascii_output_buffer);
  // If there was a need to add "mess=" or any other leading string, then it
  // would be prepended to the ascii buffer here before writing it to the file
  const size_t original_message_bytes = fwrite(
      ascii_output_buffer, 1, ASCII_MESSAGE_OUTPUT_SIZE >> 1, output_file);
  // Print newline character
  fwrite("\n", 1, 1, output_file);
  const size_t masked_message_bytes =
      fwrite(&ascii_output_buffer[ASCII_MESSAGE_OUTPUT_SIZE >> 1], 1,
             ASCII_MESSAGE_OUTPUT_SIZE >> 1, output_file);
  if (original_message_bytes != ASCII_MESSAGE_OUTPUT_SIZE >> 1 &&
      masked_message_bytes != ASCII_MESSAGE_OUTPUT_SIZE >> 1) {
    perror("Output file couldn't be generated correctly");
    return OUTPUT_WRITE_ERROR;
  }
  return OUTPUT_SUCCESS;
}

// Since output file writing errors can happen, like not opening a file, then
// they are just printed to the stderr with perror.
output_status_t write_message_output(message_out_t message_output) {
#ifdef TESTING_FLAG
#define TEST_PREFIX "test_"
#else
#define TEST_PREFIX ""
#endif
  char sample_file_path[sizeof(TEST_PREFIX) + sizeof(OUTPUT_FILENAME) + 1] =
      TEST_PREFIX;
  strcat(sample_file_path, OUTPUT_FILENAME);

  // A new file is created each time with results
  FILE *output_file = fopen(sample_file_path, "w");
  if (!output_file) {
    perror("Error: while opening output file");
    return OUTPUT_CLOSE_ERROR;
  }

  const output_status_t write_status =
      write_ascii_output((uint32_t *)&message_output, output_file);
  if (write_status != OUTPUT_SUCCESS) {
    perror("Error: while writing output file");
    return write_status;
  }

  const int output_close_error = fclose(output_file);
  if (output_close_error) {
    perror("Error: closing output file");
    return OUTPUT_CLOSE_ERROR;
  }

  return OUTPUT_SUCCESS;
}

output_status_t write_error_output(const char *error_message) {
  // A new file is created each time with results
  FILE *output_file = fopen(OUTPUT_FILENAME, "w");
  if (!output_file)
    return OUTPUT_CLOSE_ERROR;

  const int output_write_error =
      fprintf(output_file, "Error: %s\n", error_message);
  if (output_write_error) {
    perror("Couldn't write error message to output file");
  }

  const int output_close_error = fclose(output_file);
  if (output_close_error) {
    perror("Error closing output file");
    return OUTPUT_CLOSE_ERROR;
  }

  return OUTPUT_SUCCESS;
}