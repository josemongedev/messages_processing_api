#ifndef MASK_PARSE_H
#define MASK_PARSE_H

#include <messages/ascii_convert.h>
#include <messages/message_types.h>
#include <stdio.h>
#include <string.h>

#define MASK_LEAD_STR "mask="
#define LEAD_MASK_LENGTH (sizeof(MASK_LEAD_STR))

extern const char leading_mask_pattern[];

inp_status_t check_mask_lead_str(char *ascii_line, char **mask_start_ptr);

inp_status_t parse_input_mask(char *mask_raw, uint32_t *mask);

#endif /* MASK_PARSE_H */
