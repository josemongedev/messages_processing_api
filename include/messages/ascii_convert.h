#ifndef ASCII_CONVERT_H
#define ASCII_CONVERT_H

#include <endian.h>
#include <messages/message_types.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ASCII_CHARS_WORD32 8

void convert_ascii_hex_to_mask(char *ascii_buffer, uint32_t *mask);

void convert_ascii_hex_to_frame(char *ascii_buffer, uint32_t *message);

void convert_mask_to_ascii_hex(uint32_t mask, char *ascii_buffer);

void convert_frame_to_ascii_hex(uint32_t *frame, size_t bytes_length,
                                char *ascii_buffer);
#endif /* ASCII_CONVERT_H */