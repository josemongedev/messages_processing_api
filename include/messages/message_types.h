#ifndef MESSAGE_TYPES_H
#define MESSAGE_TYPES_H
#include <stdint.h>

#define PAYLOAD_DATA_LENGTH 252

typedef struct payload_s {
  uint8_t data[PAYLOAD_DATA_LENGTH];
  uint32_t CRC32;
} payload_t;

// Packed attribute is used in order to separate the 2 bytes padding that makes
// message_t reach 260 bytes(multiple of 4).
typedef struct message_input_s {
  uint8_t type;
  uint8_t length;
  payload_t payload;
} __attribute__((__packed__)) message_t;
#define MESSAGE_SIZE (sizeof(message_t))
// All ascii sizes are multiplied by 2, cause it takes 2 chars to represent a
// single byte
#define MESSAGE_ASCII_SIZE (MESSAGE_SIZE * 2)
#define ALIGNMENT_PADDING_BYTES 4
#define MESSAGE_ASCII_WITH_LEAD_SIZE (MESSAGE_ASCII_SIZE + LEAD_MESSAGE_LENGTH)

// So this structure includes the padding, and it's easier to handle
typedef struct message_padded_s {
  message_t message;
  uint16_t padding; // unused, just for discrimination purposes
} message_padded_t;
#define MESSAGE_PADDED_SIZE (sizeof(message_padded_t))
#define MESSAGE_ASCII_PADDED_SIZE (MESSAGE_PADDED_SIZE * 2)
#define MESSAGE_ASCII_PADDED_WITH_LEAD_SIZE                                    \
  (MESSAGE_ASCII_PADDED_SIZE + LEAD_MESSAGE_LENGTH)

typedef uint32_t mask_t;
#define MASK_SIZE sizeof(mask_t)
#define MASK_ASCII_SIZE (MASK_SIZE * 2)
#define MASK_ASCII_WITH_LEAD_SIZE (MASK_ASCII_SIZE + LEAD_MASK_LENGTH)

typedef struct message_mask_pair_s {
  message_padded_t message;
  mask_t mask;
} input_pair_t;

typedef struct message_output_s {
  message_t original_message;
  message_t masked_message;
} message_out_t;

#define MESSAGE_OUTPUT_SIZE (sizeof(message_out_t))
#define ASCII_MESSAGE_OUTPUT_SIZE (MESSAGE_OUTPUT_SIZE * 2)

typedef enum read_input_status_e {
  INPUT_SUCCESS = 0,
  INPUT_OPEN_ERROR = 1,
  INPUT_READ_ERROR = 2,
  INPUT_CLOSE_ERROR = 3,
  INPUT_MESSAGE_LEAD_MISSING_ERROR = 4,
  INPUT_MASK_LEAD_MISSING_ERROR = 5,
  INPUT_LEAD_STR_MISSING_ERROR = 6,
  INPUT_INVALID_MESSAGE_LENGTH = 7,
  INPUT_INVALID_MASK_LENGTH = 8,
  INPUT_INVALID_PAYLOAD_DATA_LENGTH_FORMAT = 9,
  INPUT_CRC_MISMATCH = 10,
  INPUT_TOTAL_ERROR_TYPES = 11,
} inp_status_t;

typedef enum write_output_status_e {
  OUTPUT_SUCCESS = 0,
  OUTPUT_OPEN_ERROR = 1,
  OUTPUT_WRITE_ERROR = 2,
  OUTPUT_CLOSE_ERROR = 3
} output_status_t;

#endif /* MESSAGE_TYPES_H */
