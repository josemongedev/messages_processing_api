//  Output information format (for each message):
// - message type
// - initial message length
// - initial message data bytes
// - initial CRC-32
// - modified message length
// - modified message data bytes with mask
// - modified CRC-32
// - if output file exist, it have to be updated with new information
// - all errors have to be saved in output file
// - all data stored in ASCII hex format
// - output file name - data_out.txt

#ifndef MESSAGE_OUTPUT_H
#define MESSAGE_OUTPUT_H
#include <messages/ascii_convert.h>
#include <messages/crc_32.h>
#include <messages/message_types.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define OUTPUT_FILENAME "data_out.txt"

void process_message(input_pair_t *input_pair, message_out_t *message_out);

output_status_t write_message_output(message_out_t message_output);

output_status_t write_error_output(const char *error_message);

#endif /* MESSAGE_OUTPUT_H */