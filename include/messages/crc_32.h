#ifndef CRC_32_H
#define CRC_32_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define POLYNOMIAL 0x04C11DB7;
// This is just the previous polynomial in the reversed representation formed,
// which is used with an lsb-first algorithm (the one implemented)
#define REVERSED_POLYNOMIAL 0xEDB88320
#define CRC32_SEED 0xFFFFFFFF;
#define BYTE_8_BITS_LONG 8
#define LSB_BIT_32 ((uint32_t)0x1)

uint32_t calculate_data_CRC_32(uint8_t data[], size_t length);

#endif /* CRC_32_H */