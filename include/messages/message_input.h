#ifndef MESSAGE_INPUT_H
#define MESSAGE_INPUT_H
#include <limits.h>
#include <messages/crc_32.h>
#include <messages/mask_parse.h>
#include <messages/message_parse.h>
#include <messages/message_types.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define INPUT_FILENAME "data_in.txt"

extern const char *input_error_message[INPUT_TOTAL_ERROR_TYPES];

inp_status_t read_raw_input_frame(char *message, char *mask);

inp_status_t read_input_pair(input_pair_t *input_pair);

#endif /* MESSAGE_INPUT_H */