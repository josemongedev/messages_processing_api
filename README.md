## How to use: Instructions

- On the terminal, change directory to the project root dir where build.sh and run_tests.sh are located.
- Run ***./build.sh*** first and wait for it to complete.
- Run ***./run_tests.sh*** to run all tests.
- Run ***./run_process_msg.sh*** to run the application.

## Comments/assumptions:
- When instructed to updated the output file when a new input message is read, I'm assuming the file contents needs to be overwritten ("w" mode parameter), not appended ("a" mode parameter).
- No leading strings indicated for output message structure, so I'm omitting them.
- It is not specified, but the input frame from the input hex file it's assumed to be lines separated by the newline character. So in the following format:
```
mess=<ASCII-Message><new-line-char>
mask=<ASCII-Mask> 
```
